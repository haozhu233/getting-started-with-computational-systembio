{
 "interproscan-version": "5.36-75.0",
"results": [ {
  "sequence" : "MELFLAGRRVLVTGAGKGIGRGTVQALHATGARVVAVSRTQADLDSLVRECPGIEPVCVDLGDWEATERALGSVGPVDLLVNNAAVALLQPFLEVTKEAFDRSFEVNLRAVIQVSQIVARGLIARGVPGAIVNVSSQCSQRAVTNHSVYCSTKGALDMLTKVMALELGPHKIRVNAVNPTVVMTSMGQATWSDPHKAKTMLNRIPLGKFAEVEHVVNAILFLLSDRSGMTTGSTLPVEGGFWAC",
  "md5" : "23b85f42831bcb2cf8231e9c35081628",
  "matches" : [ {
    "signature" : {
      "accession" : "SSF51735",
      "name" : "NAD(P)-binding Rossmann-fold domains",
      "description" : null,
      "signatureLibraryRelease" : {
        "library" : "SUPERFAMILY",
        "version" : "1.75"
      },
      "entry" : {
        "accession" : "IPR036291",
        "name" : "NAD(P)-bd_dom_sf",
        "description" : "NAD(P)-binding domain superfamily",
        "type" : "HOMOLOGOUS_SUPERFAMILY",
        "goXRefs" : [ ],
        "pathwayXRefs" : [ ]
      }
    },
    "locations" : [ {
      "start" : 7,
      "end" : 243,
      "hmmLength" : 329,
      "location-fragments" : [ {
        "start" : 7,
        "end" : 243,
        "dc-status" : "CONTINUOUS"
      } ]
    } ],
    "evalue" : 1.26E-73,
    "model-ac" : "0049902"
  }, {
    "signature" : {
      "accession" : "PS00061",
      "name" : "ADH_SHORT",
      "description" : "Short-chain dehydrogenases/reductases family signature.",
      "signatureLibraryRelease" : {
        "library" : "PROSITE_PATTERNS",
        "version" : "2019_01"
      },
      "entry" : {
        "accession" : "IPR020904",
        "name" : "Sc_DH/Rdtase_CS",
        "description" : "Short-chain dehydrogenase/reductase, conserved site",
        "type" : "CONSERVED_SITE",
        "goXRefs" : [ {
          "name" : "oxidoreductase activity",
          "databaseName" : "GO",
          "category" : "MOLECULAR_FUNCTION",
          "id" : "GO:0016491"
        } ],
        "pathwayXRefs" : [ ]
      }
    },
    "locations" : [ {
      "start" : 136,
      "end" : 164,
      "level" : "STRONG",
      "cigarAlignment" : "1M9I1M2I8M1I3M1I3M",
      "alignment" : "SqcsqravtnHsvYCSTKGALdMLTkVMA",
      "location-fragments" : [ {
        "start" : 136,
        "end" : 164,
        "dc-status" : "CONTINUOUS"
      } ]
    } ],
    "model-ac" : "PS00061"
  }, {
    "signature" : {
      "accession" : "PF13561",
      "name" : "adh_short_C2",
      "description" : "Enoyl-(Acyl carrier protein) reductase",
      "signatureLibraryRelease" : {
        "library" : "PFAM",
        "version" : "32.0"
      },
      "entry" : null
    },
    "locations" : [ {
      "start" : 14,
      "end" : 241,
      "hmmStart" : 1,
      "hmmEnd" : 233,
      "hmmLength" : 234,
      "hmmBounds" : "N_TERMINAL_COMPLETE",
      "evalue" : 1.4E-55,
      "score" : 188.3,
      "envelopeStart" : 14,
      "envelopeEnd" : 242,
      "postProcessed" : true,
      "location-fragments" : [ {
        "start" : 14,
        "end" : 241,
        "dc-status" : "CONTINUOUS"
      } ]
    } ],
    "evalue" : 1.2E-55,
    "score" : 188.6,
    "model-ac" : "PF13561"
  }, {
    "signature" : {
      "accession" : "cd05351",
      "name" : "XR_like_SDR_c",
      "description" : "XR_like_SDR_c",
      "signatureLibraryRelease" : {
        "library" : "CDD",
        "version" : "3.17"
      },
      "entry" : null
    },
    "locations" : [ {
      "start" : 1,
      "end" : 244,
      "sites" : [ {
        "description" : "active site",
        "numLocations" : 4,
        "siteLocations" : [ {
          "start" : 107,
          "end" : 107,
          "residue" : "N"
        }, {
          "start" : 149,
          "end" : 149,
          "residue" : "Y"
        }, {
          "start" : 136,
          "end" : 136,
          "residue" : "S"
        }, {
          "start" : 153,
          "end" : 153,
          "residue" : "K"
        } ]
      }, {
        "description" : "NADP binding site",
        "numLocations" : 32,
        "siteLocations" : [ {
          "start" : 86,
          "end" : 86,
          "residue" : "V"
        }, {
          "start" : 182,
          "end" : 182,
          "residue" : "V"
        }, {
          "start" : 18,
          "end" : 18,
          "residue" : "G"
        }, {
          "start" : 135,
          "end" : 135,
          "residue" : "S"
        }, {
          "start" : 16,
          "end" : 16,
          "residue" : "G"
        }, {
          "start" : 106,
          "end" : 106,
          "residue" : "V"
        }, {
          "start" : 84,
          "end" : 84,
          "residue" : "A"
        }, {
          "start" : 181,
          "end" : 181,
          "residue" : "V"
        }, {
          "start" : 85,
          "end" : 85,
          "residue" : "A"
        }, {
          "start" : 136,
          "end" : 136,
          "residue" : "S"
        }, {
          "start" : 17,
          "end" : 17,
          "residue" : "K"
        }, {
          "start" : 187,
          "end" : 187,
          "residue" : "G"
        }, {
          "start" : 186,
          "end" : 186,
          "residue" : "M"
        }, {
          "start" : 60,
          "end" : 60,
          "residue" : "D"
        }, {
          "start" : 179,
          "end" : 179,
          "residue" : "P"
        }, {
          "start" : 61,
          "end" : 61,
          "residue" : "L"
        }, {
          "start" : 180,
          "end" : 180,
          "residue" : "T"
        }, {
          "start" : 62,
          "end" : 62,
          "residue" : "G"
        }, {
          "start" : 40,
          "end" : 40,
          "residue" : "T"
        }, {
          "start" : 134,
          "end" : 134,
          "residue" : "V"
        }, {
          "start" : 185,
          "end" : 185,
          "residue" : "S"
        }, {
          "start" : 102,
          "end" : 102,
          "residue" : "R"
        }, {
          "start" : 149,
          "end" : 149,
          "residue" : "Y"
        }, {
          "start" : 59,
          "end" : 59,
          "residue" : "V"
        }, {
          "start" : 38,
          "end" : 38,
          "residue" : "S"
        }, {
          "start" : 43,
          "end" : 43,
          "residue" : "D"
        }, {
          "start" : 83,
          "end" : 83,
          "residue" : "N"
        }, {
          "start" : 19,
          "end" : 19,
          "residue" : "I"
        }, {
          "start" : 39,
          "end" : 39,
          "residue" : "R"
        }, {
          "start" : 14,
          "end" : 14,
          "residue" : "G"
        }, {
          "start" : 153,
          "end" : 153,
          "residue" : "K"
        }, {
          "start" : 184,
          "end" : 184,
          "residue" : "T"
        } ]
      }, {
        "description" : "homodimer interface",
        "numLocations" : 34,
        "siteLocations" : [ {
          "start" : 115,
          "end" : 115,
          "residue" : "S"
        }, {
          "start" : 147,
          "end" : 147,
          "residue" : "S"
        }, {
          "start" : 163,
          "end" : 163,
          "residue" : "M"
        }, {
          "start" : 91,
          "end" : 91,
          "residue" : "P"
        }, {
          "start" : 165,
          "end" : 165,
          "residue" : "L"
        }, {
          "start" : 140,
          "end" : 140,
          "residue" : "Q"
        }, {
          "start" : 104,
          "end" : 104,
          "residue" : "F"
        }, {
          "start" : 139,
          "end" : 139,
          "residue" : "S"
        }, {
          "start" : 120,
          "end" : 120,
          "residue" : "R"
        }, {
          "start" : 93,
          "end" : 93,
          "residue" : "L"
        }, {
          "start" : 142,
          "end" : 142,
          "residue" : "A"
        }, {
          "start" : 162,
          "end" : 162,
          "residue" : "V"
        }, {
          "start" : 64,
          "end" : 64,
          "residue" : "W"
        }, {
          "start" : 101,
          "end" : 101,
          "residue" : "D"
        }, {
          "start" : 109,
          "end" : 109,
          "residue" : "R"
        }, {
          "start" : 141,
          "end" : 141,
          "residue" : "R"
        }, {
          "start" : 150,
          "end" : 150,
          "residue" : "C"
        }, {
          "start" : 155,
          "end" : 155,
          "residue" : "A"
        }, {
          "start" : 159,
          "end" : 159,
          "residue" : "L"
        }, {
          "start" : 166,
          "end" : 166,
          "residue" : "E"
        }, {
          "start" : 100,
          "end" : 100,
          "residue" : "F"
        }, {
          "start" : 151,
          "end" : 151,
          "residue" : "S"
        }, {
          "start" : 123,
          "end" : 123,
          "residue" : "I"
        }, {
          "start" : 138,
          "end" : 138,
          "residue" : "C"
        }, {
          "start" : 112,
          "end" : 112,
          "residue" : "I"
        }, {
          "start" : 97,
          "end" : 97,
          "residue" : "K"
        }, {
          "start" : 161,
          "end" : 161,
          "residue" : "K"
        }, {
          "start" : 148,
          "end" : 148,
          "residue" : "V"
        }, {
          "start" : 154,
          "end" : 154,
          "residue" : "G"
        }, {
          "start" : 92,
          "end" : 92,
          "residue" : "F"
        }, {
          "start" : 95,
          "end" : 95,
          "residue" : "V"
        }, {
          "start" : 116,
          "end" : 116,
          "residue" : "Q"
        }, {
          "start" : 158,
          "end" : 158,
          "residue" : "M"
        }, {
          "start" : 108,
          "end" : 108,
          "residue" : "L"
        } ]
      }, {
        "description" : "homotetramer interface",
        "numLocations" : 61,
        "siteLocations" : [ {
          "start" : 115,
          "end" : 115,
          "residue" : "S"
        }, {
          "start" : 91,
          "end" : 91,
          "residue" : "P"
        }, {
          "start" : 165,
          "end" : 165,
          "residue" : "L"
        }, {
          "start" : 221,
          "end" : 221,
          "residue" : "F"
        }, {
          "start" : 213,
          "end" : 213,
          "residue" : "E"
        }, {
          "start" : 240,
          "end" : 240,
          "residue" : "G"
        }, {
          "start" : 104,
          "end" : 104,
          "residue" : "F"
        }, {
          "start" : 139,
          "end" : 139,
          "residue" : "S"
        }, {
          "start" : 120,
          "end" : 120,
          "residue" : "R"
        }, {
          "start" : 231,
          "end" : 231,
          "residue" : "T"
        }, {
          "start" : 226,
          "end" : 226,
          "residue" : "R"
        }, {
          "start" : 101,
          "end" : 101,
          "residue" : "D"
        }, {
          "start" : 109,
          "end" : 109,
          "residue" : "R"
        }, {
          "start" : 155,
          "end" : 155,
          "residue" : "A"
        }, {
          "start" : 159,
          "end" : 159,
          "residue" : "L"
        }, {
          "start" : 100,
          "end" : 100,
          "residue" : "F"
        }, {
          "start" : 151,
          "end" : 151,
          "residue" : "S"
        }, {
          "start" : 138,
          "end" : 138,
          "residue" : "C"
        }, {
          "start" : 169,
          "end" : 169,
          "residue" : "P"
        }, {
          "start" : 164,
          "end" : 164,
          "residue" : "A"
        }, {
          "start" : 217,
          "end" : 217,
          "residue" : "N"
        }, {
          "start" : 243,
          "end" : 243,
          "residue" : "A"
        }, {
          "start" : 148,
          "end" : 148,
          "residue" : "V"
        }, {
          "start" : 154,
          "end" : 154,
          "residue" : "G"
        }, {
          "start" : 208,
          "end" : 208,
          "residue" : "K"
        }, {
          "start" : 242,
          "end" : 242,
          "residue" : "W"
        }, {
          "start" : 95,
          "end" : 95,
          "residue" : "V"
        }, {
          "start" : 207,
          "end" : 207,
          "residue" : "G"
        }, {
          "start" : 158,
          "end" : 158,
          "residue" : "M"
        }, {
          "start" : 108,
          "end" : 108,
          "residue" : "L"
        }, {
          "start" : 210,
          "end" : 210,
          "residue" : "A"
        }, {
          "start" : 147,
          "end" : 147,
          "residue" : "S"
        }, {
          "start" : 203,
          "end" : 203,
          "residue" : "R"
        }, {
          "start" : 230,
          "end" : 230,
          "residue" : "T"
        }, {
          "start" : 163,
          "end" : 163,
          "residue" : "M"
        }, {
          "start" : 140,
          "end" : 140,
          "residue" : "Q"
        }, {
          "start" : 206,
          "end" : 206,
          "residue" : "L"
        }, {
          "start" : 229,
          "end" : 229,
          "residue" : "M"
        }, {
          "start" : 238,
          "end" : 238,
          "residue" : "E"
        }, {
          "start" : 244,
          "end" : 244,
          "residue" : "C"
        }, {
          "start" : 93,
          "end" : 93,
          "residue" : "L"
        }, {
          "start" : 142,
          "end" : 142,
          "residue" : "A"
        }, {
          "start" : 162,
          "end" : 162,
          "residue" : "V"
        }, {
          "start" : 168,
          "end" : 168,
          "residue" : "G"
        }, {
          "start" : 64,
          "end" : 64,
          "residue" : "W"
        }, {
          "start" : 237,
          "end" : 237,
          "residue" : "V"
        }, {
          "start" : 141,
          "end" : 141,
          "residue" : "R"
        }, {
          "start" : 150,
          "end" : 150,
          "residue" : "C"
        }, {
          "start" : 166,
          "end" : 166,
          "residue" : "E"
        }, {
          "start" : 123,
          "end" : 123,
          "residue" : "I"
        }, {
          "start" : 112,
          "end" : 112,
          "residue" : "I"
        }, {
          "start" : 97,
          "end" : 97,
          "residue" : "K"
        }, {
          "start" : 161,
          "end" : 161,
          "residue" : "K"
        }, {
          "start" : 218,
          "end" : 218,
          "residue" : "A"
        }, {
          "start" : 205,
          "end" : 205,
          "residue" : "P"
        }, {
          "start" : 228,
          "end" : 228,
          "residue" : "G"
        }, {
          "start" : 92,
          "end" : 92,
          "residue" : "F"
        }, {
          "start" : 239,
          "end" : 239,
          "residue" : "G"
        }, {
          "start" : 116,
          "end" : 116,
          "residue" : "Q"
        }, {
          "start" : 236,
          "end" : 236,
          "residue" : "P"
        }, {
          "start" : 214,
          "end" : 214,
          "residue" : "H"
        } ]
      } ],
      "evalue" : 6.1561E-170,
      "score" : 466.177,
      "location-fragments" : [ {
        "start" : 1,
        "end" : 244,
        "dc-status" : "CONTINUOUS"
      } ]
    } ],
    "model-ac" : "cd05351"
  }, {
    "signature" : {
      "accession" : "PR00080",
      "name" : "SDRFAMILY",
      "description" : "Short-chain dehydrogenase/reductase (SDR) superfamily signature",
      "signatureLibraryRelease" : {
        "library" : "PRINTS",
        "version" : "42.0"
      },
      "entry" : {
        "accession" : "IPR002347",
        "name" : "SDR_fam",
        "description" : "Short-chain dehydrogenase/reductase SDR",
        "type" : "FAMILY",
        "goXRefs" : [ ],
        "pathwayXRefs" : [ ]
      }
    },
    "locations" : [ {
      "start" : 149,
      "end" : 168,
      "pvalue" : 3.26E-6,
      "score" : 32.17,
      "motifNumber" : 3,
      "location-fragments" : [ {
        "start" : 149,
        "end" : 168,
        "dc-status" : "CONTINUOUS"
      } ]
    }, {
      "start" : 129,
      "end" : 137,
      "pvalue" : 1.3E-4,
      "score" : 52.82,
      "motifNumber" : 2,
      "location-fragments" : [ {
        "start" : 129,
        "end" : 137,
        "dc-status" : "CONTINUOUS"
      } ]
    }, {
      "start" : 75,
      "end" : 86,
      "pvalue" : 1.13E-6,
      "score" : 51.01,
      "motifNumber" : 1,
      "location-fragments" : [ {
        "start" : 75,
        "end" : 86,
        "dc-status" : "CONTINUOUS"
      } ]
    } ],
    "evalue" : 9.9E-10,
    "graphscan" : "III",
    "model-ac" : "PR00080"
  }, {
    "signature" : {
      "accession" : "PR00081",
      "name" : "GDHRDH",
      "description" : "Glucose/ribitol dehydrogenase family signature",
      "signatureLibraryRelease" : {
        "library" : "PRINTS",
        "version" : "42.0"
      },
      "entry" : {
        "accession" : "IPR002347",
        "name" : "SDR_fam",
        "description" : "Short-chain dehydrogenase/reductase SDR",
        "type" : "FAMILY",
        "goXRefs" : [ ],
        "pathwayXRefs" : [ ]
      }
    },
    "locations" : [ {
      "start" : 123,
      "end" : 139,
      "pvalue" : 1.97E-4,
      "score" : 28.87,
      "motifNumber" : 3,
      "location-fragments" : [ {
        "start" : 123,
        "end" : 139,
        "dc-status" : "CONTINUOUS"
      } ]
    }, {
      "start" : 9,
      "end" : 26,
      "pvalue" : 2.88E-8,
      "score" : 42.41,
      "motifNumber" : 1,
      "location-fragments" : [ {
        "start" : 9,
        "end" : 26,
        "dc-status" : "CONTINUOUS"
      } ]
    }, {
      "start" : 75,
      "end" : 86,
      "pvalue" : 1.03E-6,
      "score" : 51.95,
      "motifNumber" : 2,
      "location-fragments" : [ {
        "start" : 75,
        "end" : 86,
        "dc-status" : "CONTINUOUS"
      } ]
    }, {
      "start" : 170,
      "end" : 187,
      "pvalue" : 1.12E-6,
      "score" : 40.1,
      "motifNumber" : 5,
      "location-fragments" : [ {
        "start" : 170,
        "end" : 187,
        "dc-status" : "CONTINUOUS"
      } ]
    }, {
      "start" : 205,
      "end" : 225,
      "pvalue" : 6.88E-9,
      "score" : 32.8,
      "motifNumber" : 6,
      "location-fragments" : [ {
        "start" : 205,
        "end" : 225,
        "dc-status" : "CONTINUOUS"
      } ]
    }, {
      "start" : 149,
      "end" : 168,
      "pvalue" : 8.84E-8,
      "score" : 37.11,
      "motifNumber" : 4,
      "location-fragments" : [ {
        "start" : 149,
        "end" : 168,
        "dc-status" : "CONTINUOUS"
      } ]
    } ],
    "evalue" : 3.2E-33,
    "graphscan" : "IIiIII",
    "model-ac" : "PR00081"
  }, {
    "signature" : {
      "accession" : "PTHR44252:SF2",
      "name" : "L-XYLULOSE REDUCTASE",
      "description" : null,
      "signatureLibraryRelease" : {
        "library" : "PANTHER",
        "version" : "14.1"
      },
      "entry" : null
    },
    "locations" : [ {
      "start" : 1,
      "end" : 244,
      "hmmStart" : 1,
      "hmmEnd" : 244,
      "hmmLength" : 244,
      "hmmBounds" : "COMPLETE",
      "envelopeStart" : 1,
      "envelopeEnd" : 244,
      "location-fragments" : [ {
        "start" : 1,
        "end" : 244,
        "dc-status" : "CONTINUOUS"
      } ]
    } ],
    "evalue" : 0.0,
    "familyName" : "Not available",
    "score" : 525.6,
    "model-ac" : "PTHR44252:SF2"
  }, {
    "signature" : {
      "accession" : "G3DSA:3.40.50.720",
      "name" : null,
      "description" : null,
      "signatureLibraryRelease" : {
        "library" : "GENE3D",
        "version" : "4.2.0"
      },
      "entry" : null
    },
    "locations" : [ {
      "start" : 1,
      "end" : 243,
      "hmmStart" : 5,
      "hmmEnd" : 241,
      "hmmLength" : 244,
      "hmmBounds" : "COMPLETE",
      "evalue" : 6.9E-79,
      "score" : 266.6,
      "envelopeStart" : 1,
      "envelopeEnd" : 243,
      "postProcessed" : true,
      "location-fragments" : [ {
        "start" : 1,
        "end" : 243,
        "dc-status" : "CONTINUOUS"
      } ]
    } ],
    "evalue" : 5.8E-79,
    "score" : 266.9,
    "model-ac" : "3d3wA00"
  }, {
    "signature" : {
      "accession" : "PTHR44252",
      "name" : "FAMILY NOT NAMED",
      "description" : null,
      "signatureLibraryRelease" : {
        "library" : "PANTHER",
        "version" : "14.1"
      },
      "entry" : null
    },
    "locations" : [ {
      "start" : 1,
      "end" : 244,
      "hmmStart" : 1,
      "hmmEnd" : 244,
      "hmmLength" : 244,
      "hmmBounds" : "COMPLETE",
      "envelopeStart" : 1,
      "envelopeEnd" : 244,
      "location-fragments" : [ {
        "start" : 1,
        "end" : 244,
        "dc-status" : "CONTINUOUS"
      } ]
    } ],
    "evalue" : 0.0,
    "familyName" : "Not available",
    "score" : 525.6,
    "model-ac" : "PTHR44252"
  } ],
  "xref" : [ {
    "name" : "EMBOSS_001",
    "id" : "EMBOSS_001"
  } ]
} ]
}
